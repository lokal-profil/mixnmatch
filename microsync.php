#!/usr/bin/php
<?PHP

require_once ( 'public_html/php/common.php' ) ;
error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
require_once ( 'opendb.inc' ) ; // $db = openMixNMatchDB() ;
require_once ( '../listeria/Chris-G-botclasses/botclasses.php' );


function getLinkedExternalID ( $catalog , $extid ) {
	global $db ;
	$sql = "SELECT * FROM entry WHERE catalog=$catalog AND ext_id='" . $db->real_escape_string($extid) . "'" ;
	if(!$result = $db->query($sql)) die('There was an error running the query X [' . $db->error . ']'."\n$sql\n\n");
	while($o = $result->fetch_object()){
		if ( $o->ext_url != '' ) return "[" . $o->ext_url . " $extid]" ;
	}
	return "''$extid''" ; // Fallback
}

function formatLabel ( $label ) {
	$label = trim ( $label ) ;
	if ( !preg_match ( '/[a-z]/' , $label ) ) $label = ucwords ( strtolower ( $label )  ) ; // First letter uppercase, if no lower-case letters detected
	$label = preg_replace ( '/ +/' , ' ' , $label ) ; // Remove multiple spaces
	return '"' . $label . '"' ;
}

function checkCatalog ( $catalog , $prop ) {
	global $db , $logs , $qs , $catalogs ;
	
	$db = openMixNMatchDB() ;
	$lang = $catalogs[$catalog]->search_wp ;
	$extid2q = array() ;
	$sql = "SELECT * FROM entry WHERE catalog=$catalog" ;
	if(!$result = $db->query($sql)) die('There was an error running the query A [' . $db->error . ']'."\n$sql\n\n");
	while($o = $result->fetch_object()){
		$extid2q[$o->ext_id] = $o ;
	}
	
	$hadthat = array() ;
	$j = getSPARQL ( "SELECT ?item ?value { ?item wdt:P$prop ?value }" ) ;
	
	// Fix deleted/redirected items
	$items = array() ;
	foreach ( $j->results->bindings AS $d ) {
		if ( !preg_match ( '/\/Q(\d+)$/' , $d->item->value , $m ) or $d->item->type != 'uri' or $d->value->type != 'literal' ) continue ;
		$q = $m[1] ;
		$items[] = "Q$q" ;
	}
	print "Testing " . count($items) . ' - ' ;
	if ( count($items) == 0 ) { print "NONE\n" ; return ; }
	$sql = "SELECT DISTINCT page_title FROM page WHERE page_namespace=0 AND page_is_redirect=0 AND page_title IN ('" . implode("','",$items)."')" ;
	unset($items) ;
	$use_q = array() ;
	$dbwd = openDB ( 'wikidata' , 'wikidata' ) ;
	if(!$result = $dbwd->query($sql)) die('There was an error running the query X [' . $dbwd->error . ']'."\n$sql\n\n");
	while($o = $result->fetch_object()) $use_q[$o->page_title] = $o->page_title ;
	print count($use_q) . " left\n" ;
	
	
//	if ( !$db->ping() ) 
	$db = openMixNMatchDB() ;
	foreach ( $j->results->bindings AS $d ) {
		if ( !preg_match ( '/\/Q(\d+)$/' , $d->item->value , $m ) or $d->item->type != 'uri' or $d->value->type != 'literal' ) {
			print_r ( $d ) ;
			continue ;
		}
		$q = $m[1] ;
		$v = $d->value->value ;
		if ( !isset($use_q["Q$q"]) ) continue ;
		$hadthat[$v][] = $q ;
		
		if ( !isset($extid2q[$v]) ) {
			$logs['Unknown external ID'][$catalog][] = "External ID [[Q$q|" . getLinkedExternalID($catalog,$v) . "]] is not in Mix'n'match" ;
			continue ;
		}
		
		if ( $extid2q[$v]->q == $q ) { // All right!
			if ( $extid2q[$v]->user == 0 ) { // Confirmed in Wikidata, set in mix'n'match
				$ts = date ( 'YmdHis' ) ;
				$sql = "UPDATE entry SET user=4,timestamp='$ts' where id=" . $extid2q[$v]->id . " AND q=$q" ;
				if(!$result = $db->query($sql)) die('There was an error running the query B [' . $db->error . ']'."\n$sql\n\n");
			}
			continue ;
		}
		
		if ( $extid2q[$v]->q == -1 ) { // Set in Wikidata, but not in Mix'n'match.
			$ts = date ( 'YmdHis' ) ;
			$sql = "UPDATE entry SET q=$q,user=4,timestamp='$ts' where id=" . $extid2q[$v]->id . " AND (q is null or q < 0)" ;
			if(!$result = $db->query($sql)) die('There was an error running the query C [' . $db->error . ']'."\n$sql\n\n");
			$extid2q[$v]->q = $q ; // To not create a new item additionally later
			continue ;
		}

		if ( $extid2q[$v]->q != $q ) { // Mismatch
			if ( $extid2q[$v]->q== null or $extid2q[$v]->q <= 0 or $extid2q[$v]->user == 0 ) { // Just the automatcher, overwrite
				$ts = date ( 'YmdHis' ) ;
				$sql = "UPDATE entry SET q=$q,user=4,timestamp='$ts' where id=" . $extid2q[$v]->id ; // . " AND q=" . $extid2q[$v]->q ;
				if(!$result = $db->query($sql)) die('There was an error running the query D [' . $db->error . ']'."\n$sql\n\n");
			} else {
				$logs["Mismatch between Wikidata and Mix'n'match"][$catalog][] = "Wikidata says the external ID " . getLinkedExternalID($catalog,$v) . " belongs to {{Q|$q}}, but [https://tools.wmflabs.org/mix-n-match/?mode=entry&entry=".$extid2q[$v]->id." mix'n'match] says {{Q|" . $extid2q[$v]->q . "}}" ;
			}
		}
		
	}
	
	// Find multiple ID usage
	foreach ( $hadthat AS $v => $h ) {
		if ( count($h) == 1 ) continue ;
		$logs['Multiple items with same external ID'][$catalog][] = "Multiple Wikidata items with external ID " . getLinkedExternalID($catalog,$v) . " : {{Q|" . implode('}}, {{Q|',$h) . "}}" ;
	}

	// Find missing usage
	$dbwd = openDB ( 'wikidata' , 'wikidata' ) ;
	foreach ( $extid2q AS $extid => $o ) {
		$q = $o->q ;
		if ( preg_match ( '/^fake_id/' , $extid ) ) continue ;
		if ( $o->user == 0 ) continue ;
		if ( !isset($q) or $q == null or $q <= 0 ) continue ;
		if ( isset($hadthat[$extid]) ) continue ;

		$skip = false ;
		$sql = "select rev_comment from revision,page where page_title='Q$q' and rev_page=page_id AND page_namespace=0 AND rev_comment LIKE '%[[Property:P$prop]]%" . $dbwd->real_escape_string($extid) . "%' LIMIT 1" ;
#print "$sql\n" ;
		if(!$result = $dbwd->query($sql)) die('There was an error running the query Y [' . $dbwd->error . ']'."\n$sql\n\n");
		while($o = $result->fetch_object()) $skip = true ;
		if ( $skip ) continue ;

		$qs[] = "Q$q\tP$prop\t\"$extid\"" ;
#		$logs["Set in mix'n'match but not in Wikidata"][$catalog][] = "{{Q|$q}} should use " . getLinkedExternalID($catalog,$extid) . " but doesn't" ;
	}
	
	foreach ( $extid2q AS $o ) {
		if ( $o->q != -1 ) continue ;
		if ( preg_match ( '/^fake_id/' , $o->ext_id ) ) continue ;
		$qs[] = "CREATE" ;
		$qs[] = "LAST\tL$lang\t" . formatLabel($o->ext_name) ;
		if ( $lang != 'en' and $o->type == 'person' ) $qs[] = "LAST\tLen\t" . formatLabel($o->ext_name) ;
		if ( $o->type == 'person' ) $qs[] = "LAST\tP31\tQ5" ;
		$qs[] = "LAST\tP$prop\t\"{$o->ext_id}\"" ;
	}
}

# Init
$db = openMixNMatchDB() ;
#$dbwd = openDB ( 'wikidata' , 'wikidata' ) ;
$logs = array () ;
$qs = array() ;
$catalogs = array() ;
$sql = "SELECT * FROM catalog" ;
if(!$result = $db->query($sql)) die('There was an error running the query 1 [' . $db->error . ']'."\n$sql\n\n");
while($o = $result->fetch_object()) $catalogs[$o->id] = $o ;

#checkCatalog ( 23 , '652' ) ; exit ( 0 ) ; # TESTING

# Check all catalogs with property
$todo = array() ;
$sql = "SELECT * FROM catalog WHERE wd_prop is not null and wd_qual is null" ;
if(!$result = $db->query($sql)) die('There was an error running the query 2 [' . $db->error . ']'."\n$sql\n\n");
while($o = $result->fetch_object()){
	$todo[$o->id] = $o->wd_prop ;
}
foreach ( $todo AS $catalog => $prop ) {
	if ( isset($argv[1]) and $argv[1]!=$catalog ) continue ;
	print "Running catalog #" . $catalog . ", P" . $prop . "\n" ;
	checkCatalog ( $catalog , $prop ) ;
}

// Generate quick_statements file
$fh = fopen ( '/data/project/mix-n-match/public_html/qs.txt' , 'wb' ) ;
fwrite ( $fh , "\xEF\xBB\xBF" . implode ( "\n" , $qs ) ) ; # UTF8 header
fclose ( $fh ) ;

// Update Wikidata report
$max_lines = 50 ;
$ts = date ( 'Y-m-d H:i:s' ) ;
$wiki = "[https://tools.wmflabs.org/mix-n-match/ Mix'n'match] report from $ts. '''This page will be replaced daily!'''\n" ;
$wiki .= "''Please note: If you fix something from this list on Wikidata, please fix it on Mix'n'match as well, if applicable. Otherwise, the error might be re-introduced from there.''\n" ;
foreach ( $logs as $title => $v0 ) {
	$wiki .= "\n== $title ==\n" ;
	foreach ( $v0 AS $catalog => $lines ) {
		$wiki .= "=== [https://tools.wmflabs.org/mix-n-match/?mode=catalog_details&catalog=$catalog " . $catalogs[$catalog]->name . "] (" ;
		$wiki .= $catalogs[$catalog]->desc . ") ===\n" ;
		if ( count($lines) > $max_lines ) {
			$wiki .= "* " . count($lines) . " entries for this, not showing\n" ;
		} else {
			foreach ( $lines AS $l ) $wiki .= "# $l\n" ;
		}
	}
}


$ini = parse_ini_file ( '/data/project/mix-n-match/bot.ini' ) ;
$wiki_user = $ini['user'] ;
$wiki_pass = $ini['pass'] ;
$w = new wikipedia ;
$w->quiet = true ;
$w->url = "http://www.wikidata.org/w/api.php";
$w->setUserAgent( 'User-Agent: '.$wiki_user.' (http://www.wikidata.org/wiki/User:' . str_replace(' ','_',$wiki_user) . ')'  );
$w->login( $wiki_user , $wiki_pass );
$page = "User:Magnus Manske/Mix'n'match report" ;
$w->edit( str_replace(' ','_',$page), $wiki, "Update $ts" );

file_get_contents ( 'https://tools.wmflabs.org/mix-n-match/api.php?query=update_overview' ) ; // Update stats
?>