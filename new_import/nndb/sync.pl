#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;

open OUT, "> out.json" or die $! ;

my $first = 1 ;
print OUT '{"catalog":37,"entries":' ;
print OUT "[\n" ;

foreach my $num ( 0 .. 25 ) {
	my $url = "http://www.nndb.com/lists/" . (493+$num) . "/0000" . (63304+$num) . "/" ;
	my $page = get $url ;
	parse_page ( $page ) ;
}
print OUT "\n]}" ;

0 ;

sub parse_page {
	my ( $page ) = @_ ;
	my @rows = split "\n" , $page ;
	my $out = get_blank() ;
	my $cnt = 0 ;
	foreach my $r ( @rows ) {
	
		if ( $r =~ m|<td align=center valign=middle>| ) {
			if ( $cnt == 0 and $r =~ m|<a href="(http://www.nndb.com/people/)(\d+/\d+)/">(.+?)</a>| ) {
				$out->{name} = $3 ;
				$out->{id} = $2 ;
				$out->{url} = "$1$2" ;
			} elsif ( $cnt > 0 ) {
				$r =~ s|<.+?>||g ;
				push @{$out->{desc}} , $r unless $r eq '-' ;
			}
			
			$cnt++ ;
		}

		if ( $r =~ m|</tr>| ) {
			if ( defined $out->{id} ) {
				$out->{desc} = join "; " , @{$out->{desc}} ;
				if ( $first ) { $first = 0 ; }
				else { print OUT ",\n" ; }
				print OUT encode_json ( $out ) ;
			}
			$out = get_blank() ;
			$cnt = 0 ;
		}
		
	}
}

sub get_blank {
	return { 'aux' => [] , 'type' => 'person' , 'desc' => [] } ;
}