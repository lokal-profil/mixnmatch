#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;

open OUT, "> out.json" or die $! ;
my $first = 1 ;
print OUT '{"catalog":58,"entries":' ;
print OUT "[\n" ;

foreach my $from ( 1 .. 65000 ) {
	my $url = "http://opac.sbn.it/opacsbn/opaclib?db=solr_auth&rpnquery=%40attrset+bib-1++%40attr+1%3D7129+%40attr+4%3D4++%40attr++2%3D2+%222015%22&totalResult=64508&select_db=solr_auth&nentries=1&rpnlabel=+Morte%2FFine%3C%3D2015+&sortquery=+BY+%40attrset+bib-1++%40attr+1%3D1003&format=xml&searchForm=opac%2Ficcu%2Fauthority.jsp&do_cmd=search_show_cmd&sortlabel=Nome&resultForward=opac%2Ficcu%2Ffull_auth.jsp&saveparams=false&fname=none&from=$from" ;
	my $page = get $url ;
	parse_page ( $page ) ;
#last ; ##########
}

print OUT "\n]}" ;

0 ;

sub parse_page {
	my ( $page ) = @_ ;
	$page =~ s/\s+/ /g ;
	
	return unless $page =~ m|Identificativo SBN\s*</td>\s<td class="detail_value">\s*(.+?)\s*</td>| ;
	my $id = $1 ;
	
	return unless $page =~ m|Nome autore\s*</td>\s<td class="detail_value">\s*<a[^>]+>\s*(.+?)\s*</a>\s*</td>| ;
	my $name = $1 ;

	$name =~ s/&lt;/</g ;
	$name =~ s/&gt;/>/g ;
	$name =~ s/\s*<.*$// ;
	$name =~ s/^([^,]+), ([^,]+)$/$2 $1/ ;

	my @desc ;
	$page =~ m|Datazione\s*</td>\s<td class="detail_value">\s*(.+?)\s*</td>| ;
	push @desc , $1 if $1 ne '' ;
	$page =~ m|Nota informativa\s*</td>\s<td class="detail_value">\s*(.+?)\s*</td>| ;
	push @desc , $1 if $1 ne '' ;

		
	my $url = "http://opac.sbn.it/opacsbn/opac/iccu/scheda_authority.jsp?bid=$id" ;
	my $desc = join ( "; " , @desc ) ;

	my $out = {
		id => $id ,
		url => $url ,
		name => $name ,
		desc => $desc ,
		aux => [] ,
		type => 'person'
	} ;
	
	if ( $first ) { $first = 0 ; }
	else { print OUT ",\n" ; }
	my $s = encode_json ( $out ) ;
	print OUT $s ;
}
