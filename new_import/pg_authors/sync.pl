#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;

my $catalog_id = 71 ;


my $index_page_list = generate_pagelist () ;

open OUT, "> out.json" or die $! ;

my $last_id = '' ;
my $first = 1 ;
print OUT '{"catalog":' . $catalog_id . ',"entries":' ;
print OUT "[\n" ;
foreach my $url ( @{$index_page_list} ) {
	my $page = get $url ;
	parse_page ( $page , $url ) ;
}
print OUT "\n]}" ;

0 ;

sub get_blank {
	return { desc => '' , aux => [] , type => 'person' } ;
}

##########

sub generate_pagelist {
	my @letters = ( 'a' .. 'z' ) ;
	push @letters , 'other' ;
	my @pages ;
	foreach my $letter ( @letters ) {
		push @pages , "https://www.gutenberg.org/browse/authors/$letter" ;
	}
	return \@pages ;
}

sub parse_page {
	my ( $page , $url ) = @_ ;
	while ( $page =~ m|<h2><a name="(.+?)">(.+?)</a>.+?</h2>|g ) {

		my $out = get_blank() ;
		$out->{id} = $1 ;
		$out->{url} = "$url#$1" ;
		$out->{name} = $2 ;
		
		
		if ( $out->{name} =~ m|^(.+), ([0-9?-]+)$| ) {
			$out->{desc} = $2 ;
			$out->{name} = $1 ;
		}

		$out->{name} =~ s|^([^,]+), ([^,]+)$|$2 $1| ;
		
		if ( $out->{name} =~ m|^(.+) \((.+)\) (.+)$| ) {
			$out->{desc} .= "; $1 $3" ;
			$out->{name} = "$2 $3" ;
		}

		if ( $first ) { $first = 0 ; }
		else { print OUT ",\n" ; }
		print OUT encode_json ( $out ) ;
	}
}

