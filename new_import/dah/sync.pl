#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;

open OUT, "> out.json" or die $! ;

my $first = 1 ;
print OUT '{"catalog":12,"entries":' ;
print OUT "[\n" ;
foreach my $letter ( 'a' .. 'z' ) {
	my $url = "https://dictionaryofarthistorians.org/$letter.html" ;
	my $page = get $url ;
	parse_page ( $page ) ;
}
print OUT "\n]}" ;

0 ;

sub parse_page {
	my ( $page ) = @_ ;
	my @rows = split "\n" , $page ;
	my $lr = '' ;
	foreach my $r ( @rows ) {
		if ( $r =~ m|<a | and not $r =~ m|</a>| ) {
			$lr = $r ;
			next ;
		}
		if ( $r =~ m|</a>| and not $r =~ m|<a | ) {
			$r = "$lr $r" ;
		}
		if ( $r =~ m|<a href="(.+?).html">(.+?)</a>| ) {
			if ( $1 =~ m/^cite\./ or $1 eq 'recent' ) {
				$lr = '' ;
				next ;
			}
			my $out = {
				id => $1 ,
				url => "https://dictionaryofarthistorians.org/$1.html" ,
				name => $2 ,
				desc => '' ,
				aux => [] ,
				type => 'person'
			} ;
			$out->{name} =~ s/[\[\]]//g ;
			$out->{name} =~ s/,\s*pseudonym//g ;
			$out->{name} =~ s/\s+/ /g ;
			$out->{name} =~ s/^(.+), ([^,]+)$/$2 $1/ ;
			if ( $first ) { $first = 0 ; }
			else { print OUT ",\n" ; }
			print OUT encode_json ( $out ) ;
		}
		$lr = '' ;
	}
}
