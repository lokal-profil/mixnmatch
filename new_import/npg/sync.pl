#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;


open OUT, "> out.json" or die $! ;

my $last_id = '' ;
my $first = 1 ;
my %name_cache ;
print OUT '{"catalog":47,"entries":' ;
print OUT "[\n" ;

foreach my $part ( 'artA-Z/art' , 'sitA-Z/sit' ) {
	foreach my $letter ( 'a' .. 'z'  ) {
		my $url = "http://www.npg.org.uk/collections/search/$part$letter" ;
		my $page = get $url ;
		parse_page ( $page ) ;
	}
}
print OUT "\n]}" ;

0 ;

sub parse_page {
	my ( $page ) = @_ ;
	$page =~ s/\s+/ /g ;
	while ( $page =~ m|<p><a href="/collections/search/person/([^/]+?)/.*?">(.+?)</a>\s*(.*?)</p>|g ) {

		my $out = get_blank() ;
		$out->{id} = $1 ;
		$out->{url} = "http://www.npg.org.uk/collections/search/person/$1" ;
		$out->{name} = $2 ;
		$out->{desc} = $3 ;
		$out->{desc} =~ s/\.\s*\d+\s+[Pp]ortraits{0,1}$// ;
		$out->{desc} =~ s/^\((.+)\)$/$1/ ;

		if ( $first ) { $first = 0 ; }
		else { print OUT ",\n" ; }
		print OUT encode_json ( $out ) ;
	}
}

sub get_blank {
	return { desc => '' , aux => [] , type => 'person' } ;
}