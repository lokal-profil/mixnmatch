#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;

open OUT, "> out.json" or die $! ;
my $first = 1 ;
print OUT '{"catalog":59,"entries":' ;
print OUT "[\n" ;

foreach my $index ( qw { 00004 00524 02470 03015 03610 03976 04835 05728 06708 06880 07166 09007 09733 10889 11372 11562 11588 12524 12528 13281 14241 15364 16128 16227 16236 16242 16921 17156 } ) {
	my $url = "http://mek.oszk.hu/00300/00355/html/toc/$index.html" ;
	my $page = get $url ;
	while ( $page =~ m|<p class="toc"><a href="../([^/]+/\d+).htm" target="doc">(.+?)</a></p>|gm ) {
		parse_page ( "http://mek.oszk.hu/00300/00355/html/$1.htm" , $1 , $2 ) ;
	}
}

=cut
foreach my $from ( 1 .. 65000 ) {
	my $url = "http://opac.sbn.it/opacsbn/opaclib?db=solr_auth&rpnquery=%40attrset+bib-1++%40attr+1%3D7129+%40attr+4%3D4++%40attr++2%3D2+%222015%22&totalResult=64508&select_db=solr_auth&nentries=1&rpnlabel=+Morte%2FFine%3C%3D2015+&sortquery=+BY+%40attrset+bib-1++%40attr+1%3D1003&format=xml&searchForm=opac%2Ficcu%2Fauthority.jsp&do_cmd=search_show_cmd&sortlabel=Nome&resultForward=opac%2Ficcu%2Ffull_auth.jsp&saveparams=false&fname=none&from=$from" ;
	my $page = get $url ;
	parse_page ( $page ) ;
}
=cut

print OUT "\n]}" ;

0 ;

sub parse_page {
	my ( $url , $id , $name ) = @_ ;
	my $page = get $url ;
	$page =~ s/\s+/ /g ;

	my $desc = '' ;
	if ( $page =~ m|<SPAN CLASS="Szlets" field="Születés">\((.*?)</SPAN>.*?<SPAN CLASS="Hallozs" field="Halálozás">(.*?)\)</SPAN>| ) {
		$desc .= "$1 - $2" ;
	}
	
	if ( $page =~ m|<SPAN CLASS="Foglalkozs" field="Foglalkozás">(.*?)</SPAN>| ) {
		$desc .= "; $1" ;
	}
	

	my $out = {
		id => $id ,
		url => $url ,
		name => $name ,
		desc => $desc ,
		aux => [] ,
		type => 'person'
	} ;
	
	if ( $first ) { $first = 0 ; }
	else { print OUT ",\n" ; }
	my $s = encode_json ( $out ) ;
	print OUT $s ;
}
