#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;

my %ids ;
open OUT, "> out.json" or die $! ;

my $base_url = 'http://openskos.beeldengeluid.nl/oai-pmh/?verb=ListRecords' ;

my $first = 1 ;
my $token = 'dummy' ;
print OUT '{"catalog":34,"entries":' ;
print OUT "[\n" ;
while ( $token ne '' ) {
	my $url ;
	if ( $token eq 'dummy' ) { $url = "$base_url&metadataPrefix=oai_rdf&set=beng:gtaa" ; }
	else { $url = "$base_url&resumptionToken=$token" ; }
	my $xml = get $url ;
	$token = process_xml ( $xml ) ;
}
print OUT "\n]}" ;

0 ;

sub process_xml {
	my ( $xml ) = @_ ;
	my @rows = split "\n" , $xml ;
	my $ret = '' ;
	my ( $url , $id , $name , $desc ) ;
	my $use = 1 ;
	my $person = 0 ;
	foreach my $r ( @rows ) {
		if ( $r =~ m|>(.+?)</resumptionToken>| ) {
			$ret = $1 ;
			next ;
		}

		if ( $r =~ m|rdf:about="(.+?)">| ) { $url = $1 ; }
		if ( $r =~ m|<skos:notation>(\d+)</skos:notation>| ) { $id = $1 ; }
		if ( $r =~ m|<skos:prefLabel .+?>(.+?), (.+?)</skos:prefLabel>| ) { $name = "$2 $1" ; }
		if ( $r =~ m|<skos:hiddenLabel .+?>(.+?)</skos:prefLabel>| ) { $name = $1 ; }
		if ( $r =~ m|<skos:scopeNote .+?>(.+?)</skos:scopeNote>| ) { $desc = $1 unless $1 eq '{}' ; }
		if ( $r =~ m|Persoonsnamen| ) { $person = 1 ; }
		if ( $r =~ m|eader status="deleted"| ) { $use = 0 ; }
		if ( $r =~ m|</record>| ) {
			$use = 0 if $id eq '' or $name eq '' or $url eq '' ;
			if ( $use and $person ) {
				my $out = {
					'id' => $id ,
					'name' => $name ,
					'desc' => $desc ,
					'url' => $url ,
					'aux' => [] ,
					'type' => ($person?'person':'')
				} ;
				if ( $first ) { $first = 0 ; }
				else { print OUT ",\n" ; }
				print OUT encode_json ( $out ) ;
			}
			$use = 1 ;
			$name = '' ;
			$url = '' ;
			$id = '' ;
			$desc = '' ;
			$person = 0 ;
		}
	}
	return $ret ;
}
