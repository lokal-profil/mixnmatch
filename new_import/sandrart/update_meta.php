#!/usr/bin/php
<?PHP

require_once ( "../../public_html/php/common.php" ) ;

$db = openToolDB ( 'mixnmatch_p' , 'wikidatawiki.labsdb' , 'p50380g50851' ) ;
#$dbwd = openDB ( 'wikidata' , 'wikidata' ) ;
$db->set_charset("utf8") ;

$entries = array() ;
$sql = "SELECT * FROM entry WHERE catalog=9 AND ext_desc='' and (q is null or q<1)" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
while($o = $result->fetch_object()) $entries[] = $o ;

foreach ( $entries AS $o ) {
	$html = file_get_contents ( $o->ext_url ) ;
	$html = preg_replace ( '/\s+/' , ' ' , $html ) ;
	if ( !preg_match ( '/<h2>Basic data<\/h2>\s*<p>\s*(.+?)<\/p>/' , $html , $m ) ) continue ;
	$desc = $m[1] ;
	$desc = html_entity_decode ( preg_replace ( '/<.+?>/' , '' , $desc ) ) ;
//	print $o->ext_name . "\t$desc\n" ;

	$sql = "UPDATE entry SET ext_desc='" . $db->real_escape_string(trim($desc)) . "' WHERE id=" . $o->id ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
}

?>