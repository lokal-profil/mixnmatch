#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;

my $catalog_id = 89 ;

my $first = 1 ;
open OUT, "> out.json" or die $! ;
print OUT '{"catalog":' . $catalog_id . ',"entries":' ;
print OUT "[\n" ;

#batch_parse () ;
continuous_parse ( 'http://trove.nla.gov.au/people/result?q&s=$1' , 0 , 20 ) ;

print OUT "\n]}" ;

0 ;

sub get_blank {
	return { desc => '' , aux => [] , type => 'person' } ;
}

sub batch_parse {
	my $index_page_list = generate_pagelist () ;
	foreach my $url ( @{$index_page_list} ) {
		my $page = get $url ;
		parse_page ( $page , $url ) ;
	}
}

sub continuous_parse {
	my ( $url_pattern , $start , $step ) = @_ ;
	my $pos = $start ;
	while ( 1 ) {
		my $url = $url_pattern ;
		$url =~ s/\$1/$pos/g ;
		my $page = get $url ;
		my $found = parse_page ( $page ) ;
		last if $found == 0 ;
		$pos += $step ;
	}
}

##########


sub parse_page {
	my ( $page , $url ) = @_ ;
	my $ret = 0 ;
	$page =~ s/\s+/ /g ;
	
	while ( $page =~ m|<a href="/people/(\d+)\?q&amp;c=people">(.+?)</a>|g ) {

		my $out = get_blank() ;
		$out->{id} = $1 ;
		$out->{url} = "http://trove.nla.gov.au/people/$1?q&c=people" ;
		$out->{name} = $2 ;
		
		
		if ( $out->{name} =~ m|^(.+?) \((.+)\)$| ) {
			$out->{desc} = $2 ;
			$out->{name} = $1 ;
			$out->{desc} =~ s/\) \(/; /g ;
		}

		$out->{name} =~ s|,\s*$|| ;
		$out->{name} =~ s|^([^,]+), ([^,]+)$|$2 $1| ;
		
		if ( $first ) { $first = 0 ; }
		else { print OUT ",\n" ; }
		print OUT encode_json ( $out ) ;
		$ret++ ;
	}
	return $ret ;
}

