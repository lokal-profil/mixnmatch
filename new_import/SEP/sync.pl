#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;

my $catalog_id = 90 ;

my $first = 1 ;
open OUT, "> out.json" or die $! ;
print OUT '{"catalog":' . $catalog_id . ',"entries":' ;
print OUT "[\n" ;

batch_parse () ;
#continuous_parse ( 'https://www.eerstekamer.nl/oud_leden?start=$1&s01=jv5dj6y41uz601&_charset_=UTF-8&u1a=&dlgid=jv5dj6ydgglmd&' , 0 , 50 ) ;

print OUT "\n]}" ;

0 ;

sub get_blank {
	return { desc => '' , aux => [] , type => '' } ;
}

sub batch_parse {
	my $index_page_list = generate_pagelist () ;
	foreach my $url ( @{$index_page_list} ) {
		my $page = get $url ;
		parse_page ( $page , $url ) ;
	}
}

sub continuous_parse {
	my ( $url_pattern , $start , $step ) = @_ ;
	my $pos = $start ;
	while ( 1 ) {
		my $url = $url_pattern ;
		$url =~ s/\$1/$pos/g ;
		my $page = get $url ;
		my $found = parse_page ( $page ) ;
		last if $found == 0 ;
		$pos += $step ;
	}
}

##########

sub generate_pagelist {
	my @pages ;
	push @pages , 'http://plato.stanford.edu/contents.html' ;
	return \@pages ;
}

my %hadthat ;

sub parse_page {
	my ( $page , $url ) = @_ ;
	while ( $page =~ m|<li>\s*<a href="entries/([^"#]+)/">(.+?)</a>|g ) {

		my $out = get_blank() ;
		$out->{id} = $1 ;
		$out->{url} = "http://plato.stanford.edu/entries/$1/" ;
		$out->{name} = $2 ;
		
		next if defined $hadthat{$1} ;
		$hadthat{$1} = 1 ;
		
		$out->{name} =~ s|<.+?>||g ;
		if ( $out->{name} =~ m|\s*\[(.+)\]| ) {
			$out->{desc} = $1 ;
			$out->{name} =~ s|\s*\[(.+)\]|| ;
		}

		if ( $out->{name} =~ m|^(.+), (.+)$| ) {
			$out->{name} = "$2 $1" ;
			$out->{type} = 'person' ;
		}

		if ( $first ) { $first = 0 ; }
		else { print OUT ",\n" ; }
		print OUT encode_json ( $out ) ;
	}
}

