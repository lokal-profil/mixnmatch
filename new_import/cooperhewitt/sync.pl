#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;

my $catalog_id = 83 ;

my $first = 1 ;
open OUT, "> out.json" or die $! ;
print OUT '{"catalog":' . $catalog_id . ',"entries":' ;
print OUT "[\n" ;

#batch_parse () ;
continuous_parse ( 'https://collection.cooperhewitt.org/people/page$1' , 1 , 1 ) ;

print OUT "\n]}" ;

0 ;

sub get_blank {
	return { desc => '' , aux => [] , type => 'person' } ;
}

sub batch_parse {
	my $index_page_list = generate_pagelist () ;
	foreach my $url ( @{$index_page_list} ) {
		my $page = get $url ;
		parse_page ( $page , $url ) ;
	}
}

sub continuous_parse {
	my ( $url_pattern , $start , $step ) = @_ ;
	my $pos = $start ;
	while ( 1 ) {
		my $url = $url_pattern ;
		$url =~ s/\$1/$pos/g ;
		my $page = get $url ;
		my $found = parse_page ( $page , $url ) ;
		last if $found == 0 ;
		$pos += $step ;
	}
}

##########

sub parse_page {
	my ( $page , $url ) = @_ ;
	my $ret = 0 ;
	print "$url\n" ;
	while ( $page =~ m|<a href="(https://collection.cooperhewitt.org/people/)(\d+)/" class="person-title">(.+?)</a>|g ) {

		my $out = get_blank() ;
		$out->{id} = $2 ;
		$out->{url} = "$1$2" ;
		$out->{name} = $3 ;
		
		$out->{name} =~ s|^[DM]r\. || ;
		
		if ( $first ) { $first = 0 ; }
		else { print OUT ",\n" ; }
		print OUT encode_json ( $out ) ;
		$ret++ ;
	}
	return $ret ;
}

