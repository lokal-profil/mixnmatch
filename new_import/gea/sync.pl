#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;


open OUT, "> out.json" or die $! ;

my $last_id = '' ;
my $first = 1 ;
print OUT '{"catalog":45,"entries":' ;
print OUT "[\n" ;
foreach my $letter ( 'a' .. 'z' ) {
	my $cursor = 1 ;
	while ( 1 ) {
		my $url = "http://www.enciclopedia-aragonesa.com/alfa/buscar.asp?letra=$letter&cursor=$cursor" ;
		$cursor += 10 ;
		my $page = get $url ;
		my $found = parse_page ( $page ) ;
		last if $found == 0 ;
	}
}
print OUT "\n]}" ;

0 ;

sub parse_page {
	my ( $page ) = @_ ;
	my @rows = split "\n" , $page ;
	my $found = 0 ;
	my $out = get_blank() ;
	foreach my $r ( @rows ) {
	
		if ( $r =~ m|<td><h3><a href="\.\./voz\.asp\?voz_id=(\d+)" title=".*?">(.+?)</a></h3>| ) {
			$out->{id} = $1 ;
			$out->{name} = $2 ;
			$out->{url} = "http://www.enciclopedia-aragonesa.com/voz.asp?voz_id=" . $out->{id} ;
			print encode_json($out)."\n";
		} elsif ( $r =~ m/<p>(.+?)...<\/(P|td)>/ ) {
			next unless defined $out->{id} ;
			$out->{desc} = $1 ;
			if ( $first ) { $first = 0 ; }
			else { print OUT ",\n" ; }
			print OUT encode_json ( $out ) ;
			$found++ ;
		}
	}
	return $found ;
}

sub get_blank {
	return { desc => '' , aux => [] , type => '' } ;
}