#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;

my $max = 310 ;

open OUT, "> out.json" or die $! ;
print OUT '{"catalog":63,"entries":' ;
print OUT "[\n" ;

my $first = 1 ;
foreach my $num ( 1 .. $max ) {
	my $url = "http://adb.anu.edu.au/biographies/name/?page=$num&sortOrder=asc" ;
	my $page = get $url ;
	parse_page ( $page ) ;
}

print OUT "\n]}" ;

0 ;

sub parse_page {
	my ( $page ) = @_ ;
	my $cnt = 0 ;
	$page =~ s/\s+/ /g ;
	while ( $page =~ m|<li>\s*<a href="/biography/(.+?)" class="name">(.+?)</a>(.+?)</li>|g ) {
		my $out = get_blank() ;
		$out->{id} = $1 ;
		$out->{name} = $2 ;
		$out->{desc} = $3 ;
		$out->{url} = "http://adb.anu.edu.au/biography/$1" ;
		
		$out->{name} =~ s|^(.+), (.+)$|$2 $1| ;
		$out->{desc} =~ s|&ndash;|-| ;
		$out->{desc} =~ s|^\s+|| ;
		$out->{desc} =~ s|\s+$|| ;
		if ( $out->{name} =~ m|^(.+) \((.+)\)$| ) {
			$out->{name} = $1 ;
			$out->{desc} .= " $2" ;
		}

		if ( $first ) { $first = 0 ; }
		else { print OUT ",\n" ; }
		print OUT encode_json ( $out ) ;
		$cnt++ ;
	}
	return $cnt ;
}

sub get_blank {
	return { desc => '' , aux => [] , type => 'person' } ;
}