#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;

my %months = (
	'januar' => 'january' ,
	'februar' => 'february' ,
	'marec' => 'march' ,
	'maj' => 'may' ,
	'junij' => 'june' ,
	'julij' => 'july' ,
	'avgust' => 'august' ,
	'oktober' => 'october'
) ;

my @letters = ( 'Č' , 'A' .. 'Z' , 'Š' , 'Ž' ) ;

open OUT, "> out.json" or die $! ;

my $first = 1 ;
print OUT '{"catalog":38,"entries":' ;
print OUT "[\n" ;
foreach my $letter ( @letters ) {
	foreach my $num ( 1 .. 15 ) {
		my $url = "http://www.slovenska-biografija.si/abeceda/$letter/stran/$num/" ;
		my $page = get $url ;
		parse_page ( $page ) ;
	}
}
print OUT "\n]}" ;

0 ;

sub parse_page {
	my ( $page ) = @_ ;
	my @rows = split "\n" , $page ;
	my $out = get_blank() ;
	my $mode = '' ;
	foreach my $r ( @rows ) {
	
		if ( $r =~ m|<td class="gender">&#(\d+);</td>| ) {
			my $v = $1 * 1 ;
			if ( $v == 9794 ) { push @{$out->{aux}} , {prop=>"P21",id=>'Q6581097'} ; } # Male
			if ( $v == 9792 ) { push @{$out->{aux}} , {prop=>"P21",id=>'Q6581072'} ; } # Female
			push @{$out->{desc}} , "&#$v;" ;
		} elsif ( $r =~ m|<span class="arrow">| ) {
			$out->{redirect} = 1 ; # Skip
		} elsif ( $r =~ m|<div class="birth">| ) {
			$mode = 'birth' ;
		} elsif ( $r =~ m|<div class="death">| ) {
			$mode = 'death' ;
		} elsif ( $r =~ m|<div>(.+)</div>| and ( $mode eq 'birth' or $mode eq 'death' ) ) {
			push @{$out->{desc}} , "$mode: $1" ;
			$mode = '' ;
		} elsif ( $r =~ m|<a href="/poklic/.+?/">(.+?)</a>| ) {
			push @{$out->{desc}} , $1 ;
		} elsif ( $r =~ m|<a href="(/oseba/sbi)(\d+)/">(.+?)</a>| ) {
			$out->{url} = "http://www.slovenska-biografija.si$1$2" ;
			$out->{id} = $2 ;
			$out->{name} = $3 ;
			$out->{name} =~ s/^(.+), ([^,]+)$/$2 $1/ ;
		} elsif ( $r =~ m|</tr>| ) {
			if ( defined $out->{id} and not defined $out->{redirect} ) {
				$out->{desc} = join "; " , @{$out->{desc}} ;
				foreach my $ms ( keys %months ) {
					my $me = $months{$ms} ;
					$out->{desc} =~ s/\b$ms\b/$me/g ;
				}
				if ( $first ) { $first = 0 ; }
				else { print OUT ",\n" ; }
				print OUT encode_json ( $out ) ;
			}
			$out = get_blank() ;
			$mode = '' ;
		}
		
	}
}

sub get_blank {
	return { aux => [] , desc => [] , type => 'person' } ;
}