#!/usr/bin/php
<?PHP

$out = array('catalog'=>22,'entries'=>array()) ;
foreach ( array ( 'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z' ) AS $letter ) {
	foreach ( array ( 'a-f' , 'g-l' , 'm-q' , 'r-t' , 'u-z' ) AS $sub ) {
		$url = "http://clara.nmwa.org/index.php?g=artist_index&subPage=$sub&p=$letter" ;
		$t = file_get_contents ( $url ) ;
		$t = explode ( "\n" , $t ) ;
		$entry = array('aux' => array() , 'type'=>'person') ;
		foreach ( $t AS $row ) {
			$row = preg_replace ( '/<img.+?>/' , '' , $row ) ;
			if ( preg_match ( "/<a.*entity_detail&amp;entity_id=(\d+)'.*?>(.+)<\/a>/" , $row , $m ) ) {
				if ( isset($entry['id']) ) $out['entries'][] = $entry ;
				$entry = array('aux' => array() , 'type'=>'person') ;
				$entry['id'] = $m[1] ;
				$entry['name'] = $m[2] ;
				$entry['url'] = "http://clara.nmwa.org/index.php?g=entity_detail&entity_id=" . $m[1] ;
			} else if ( preg_match ( '/searchResultPortraitDetails.*?>(.+)<\/div>/' , $row , $m ) ) {
				$entry['desc'] = preg_replace ( '/\s*\|\s*/' , '; ' , $m[1] ) ;
			}
		}
		if ( isset($entry['id']) ) $out['entries'][] = $entry ;
#break ; // TESTING
	}
#break ; // TESTING
}

$fh = fopen ( "out.json" , 'w' ) ;
fwrite ( $fh , json_encode ( $out ) ) ;
fclose ( $fh ) ;

?>