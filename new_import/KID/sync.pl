#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;

my $catalog_id = 91 ;

my $first = 1 ;
open OUT, "> out.json" or die $! ;
print OUT '{"catalog":' . $catalog_id . ',"entries":' ;
print OUT "[\n" ;

my @letters = ( 'A' .. 'Z' ) ;
push @letters , 'Æ' ;
push @letters , 'Ø' ;
push @letters , 'Å' ;

#batch_parse () ;
foreach my $letter ( @letters ) {
	continuous_parse ( 'https://www.kulturarv.dk/kid/SoegKunstneroversigtRefresh.do?page=$1&hitsPerPage=100&prefix='.$letter , 0 , 1 ) ;
}

print OUT "\n]}" ;

0 ;

sub get_blank {
	return { desc => '' , aux => [] , type => 'person' } ;
}

sub batch_parse {
	my $index_page_list = generate_pagelist () ;
	foreach my $url ( @{$index_page_list} ) {
		my $page = get $url ;
		parse_page ( $page , $url ) ;
	}
}

sub continuous_parse {
	my ( $url_pattern , $start , $step ) = @_ ;
	my $pos = $start ;
	while ( 1 ) {
		my $url = $url_pattern ;
		$url =~ s/\$1/$pos/g ;
		my $page = get $url ;
		my $found = parse_page ( $page ) ;
		last if $found == 0 ;
		$pos += $step ;
	}
}

##########


sub parse_page {
	my ( $page , $url ) = @_ ;
	my $ret = 0 ;
	$page =~ s/\s+/ /g ;
	while ( $page =~ m/<tr>.+?action="(\/kid\/VisKunstner.do);jsessionid=[0-9A-F]+\?kunstnerId=(\d+)".+?>([^<>]+)<\/a>.+?<td class="[a-z]+">(.*?)<\/td>\s*<td class="[a-z]+">(.*?)<\/td>\s*<td class="[a-z]+">(.*?)<\/td>.*?<\/tr>/g ) {

		my $out = get_blank() ;
		$out->{id} = $2 ;
		$out->{url} = "https://www.kulturarv.dk$1?kunstnerId=$2" ;
		$out->{name} = $3 ;

		my @d ;
		push @d , "born: $4" if $4 ne '' ;
		push @d , "occupation: $5" if $5 ne '' ;
		push @d , "source: $6" if $6 ne '' ;
		$out->{desc} = join '; ' , @d ;
		
		$out->{name} =~ s|^([^,]+), ([^,]+)$|$2 $1| ;

		if ( $first ) { $first = 0 ; }
		else { print OUT ",\n" ; }
		print OUT encode_json ( $out ) ;
		$ret++ ;
	}
	return $ret ;
}

