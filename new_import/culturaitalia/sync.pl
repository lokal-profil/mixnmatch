#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;

my $catalog_id = 94 ;

my $first = 1 ;
open OUT, "> out.json" or die $! ;
print OUT '{"catalog":' . $catalog_id . ',"entries":' ;
print OUT "[\n" ;

open FILE , 'data.csv' ;
my $h = <FILE> ;
while ( <FILE> ) {
	chomp ;
	next if $_ eq '' ;
	next unless $_ =~ m/^(.+),(oai:culturaitalia.it:.+)$/ ;
	my ( $name , $id ) = ( $1 , $2 ) ;
	$name =~ s/^"// ;
	$name =~ s/"$// ;
	
	my $desc = '' ;
	if ( $name =~ m/^(.+), ([^,]+)$/ ) {
		$desc = $2 ;
		$name = $1 ;
	}
	
	my $o = get_blank() ;
	$o->{id} = $id ;
	$o->{name} = $name ;
	$o->{desc} = $desc ;

	if ( $first ) { $first = 0 ; }
	else { print OUT ",\n" ; }
	print OUT encode_json ( $o ) ;
}
close FILE ;


print OUT "\n]}" ;

0 ;

sub get_blank {
	return { desc => '' , aux => [] , type => 'location' } ;
}
