#!/usr/bin/php
<?PHP

require_once ( 'public_html/php/common.php' ) ;
error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
require_once ( 'opendb.inc' ) ; // $db = openMixNMatchDB() ;

$db = openMixNMatchDB() ;
$sql = "SELECT DISTINCT q FROM entry WHERE q IS NOT NULL AND q>0" ; // AND user!=0" ;
if(!$result = $db->query($sql)) die('There was an error running the query 1 [' . $db->error . ']'."\n$sql\n\n");
$qlist = "'This is not actually a real title'" ;
while($o = $result->fetch_object()){
	$qlist .= ',"Q'.$o->q.'"' ;
}

$sql = "SELECT page_title,(SELECT pl_title FROM pagelinks WHERE page_id=pl_from and pl_namespace=0 limit 1) AS target FROM page WHERE page_namespace=0 AND page_is_redirect=1 AND page_title IN ($qlist)" ;
$dbwd = openDB ( 'wikidata' , 'wikidata' ) ;
if(!$result = $dbwd->query($sql)) die('There was an error running the query 3 [' . $dbwd->error . ']'."\n$sql\n\n");
while($o = $result->fetch_object()){
	if ( !isset($o->target) or $o->target == null or $o->target == '' ) continue ;
	$fix[$o->page_title] = $o->target ;
#	print $o->page_title . "\t" . $o->target . "\n" ;
}

$db = openMixNMatchDB() ;
foreach ( $fix AS $from => $to ) {
	$from = preg_replace ( '/\D/' , '' , "$from" ) ;
	$to = preg_replace ( '/\D/' , '' , "$to" ) ;
	$sql = "UPDATE entry SET q=$to WHERE q=$from" ;
	if(!$result = $db->query($sql)) die('There was an error running the query 4 [' . $db->error . ']'."\n$sql\n\n");
}
print count($fix) . " items redirected.\n" ;

?>