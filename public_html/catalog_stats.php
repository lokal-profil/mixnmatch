<?PHP

// See https://www.wikidata.org/wiki/User:Charles_Matthews/Dictionary_of_Welsh_Biography_report
// https://www.wikidata.org/w/index.php?title=Special%3AWhatLinksHere&target=Q19595382&namespace=120

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|
ini_set('display_errors', 'On');

require_once ( 'php/common.php' ) ;
require_once ( '../opendb.inc' ) ; // $db = openMixNMatchDB() ;

function getCatPropClaim() {
	global $cat ;
	if ( $cat->id == 92 ) return 'claim[463:123885]' ;
	return 'claim[' . preg_replace ( '/\D/' , '' , $cat->wd_prop ) . ']' ;
}

function countWDQ ( $query ) {
	global $wdq_internal_url ;
	$query = getCatPropClaim() . " AND ($query)" ;
	$url = "$wdq_internal_url?q=" . urlencode($query) . "&noitems=1" ;
//	print "<pre>$url</pre>" ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
	return $j->status->items ;
}

function autolist ( $query ) {
	return "https://tools.wmflabs.org/autolist/?run=Run&wdq=" . urlencode($query) ;//. "&language=en&project=wikipedia&category=&depth=12&pagepile=&statementlist=&mode_manual=or&mode_cat=or&mode_wdq=not&mode_find=or&chunk_size=10000" ;
}

function countProps ( $prop ) {
	global $dbwd , $qs ;
	$ret = 0 ;
	$sql = "SELECT count(*) AS cnt FROM page,pagelinks where page_id=pl_from and page_namespace=0 and page_title IN ($qs) AND pl_namespace=120 AND pl_title='$prop'" ;
//	print "<pre>$sql</pre>" ;
	if(!$result = $dbwd->query($sql)) die('There was an error running the query [' . $dbwd->error . ']');
	if($o = $result->fetch_object()) $ret = $o->cnt ;
	return $ret ;
}

//____________________________________________________________________________________________________________________________________________________________________________________

$min_perc_props = get_request ( 'min_perc_props' , 10 ) * 1 ;
$min_perc_sitelinks = get_request ( 'min_perc_sitelinks' , 10 ) * 1 ;

$catalog = get_request ( 'catalog' , 0 ) * 1 ;


//____________________________________________________________________________________________________________________________________________________________________________________

print get_common_header ( '' , 'Catalog stats' ) ;

print "<div class='lead'><form method='get' class='form form-inline'>" ;
print "Catalog ID: <input type='number' name='catalog' value='$catalog' /><br/>" ;
print "Min% AC properties: <input type='text' name='min_perc_props' value='$min_perc_props' /><br/>" ;
print "Min% sitelinks: <input type='text' name='min_perc_sitelinks' value='$min_perc_sitelinks' /> " ;
print "<input type='submit' value='Show' class='btn btn-primary' />" ;
print "</form></div>" ;

if ( $catalog == 0 ) exit ( 0 ) ;

$db = openMixNMatchDB() ;
$dbwd = openDB ( 'wikidata' , 'wikidata' ) ;

$cat = array() ;
$sql = "SELECT * FROM catalog WHERE id=$catalog" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
if($o = $result->fetch_object()) $cat = $o ;

if ( !isset($cat->wd_prop) or $cat->wd_prop == '' ) die ( "This only works for catalogs with a matching Wikidata property." ) ;
if ( isset($cat->wd_qual) and $cat->wd_qual != '' ) die ( "This does not work for qualifier-based catalogs." ) ;

$ov = array() ;
$sql = "SELECT * FROM overview WHERE catalog=$catalog" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
if($o = $result->fetch_object()) $ov = $o ;
$ov->total_valid = $ov->total - $ov->na ;


$items = array() ;
$sql = "SELECT DISTINCT q FROM entry WHERE catalog=$catalog AND q>0" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()) $items[] = $o->q ;
$itemcount = count ( $items ) ;
$items = implode ( ',' , $items ) ;
$qs = '"Q' . str_replace ( ',' , '","Q' , $items ) . '"' ;

// Get authority control properties
$props = array(
	'P373' => 'Commons category' ,
	'P1472' => 'Commons Creator page' ,
	'P18' => 'Image'
) ;
$sql = "select DISTINCT page_title,term_text from wb_entity_per_page,wb_terms,page,pagelinks where page_id=epp_page_id AND term_entity_id=epp_entity_id and epp_entity_type='property' and term_entity_type='property' and term_language='en' and term_type='label' AND pl_from=page_id and page_namespace=120 and pl_namespace=0 and pl_title IN ('Q19595382','Q18614948')" ;
if(!$result = $dbwd->query($sql)) die('There was an error running the query [' . $dbwd->error . ']');
while($o = $result->fetch_object()) {
	$props[$o->page_title] = $o->term_text ;
}
$propcount = array() ;
$sql = "SELECT pl_title,count(*) AS cnt FROM page,pagelinks WHERE page_id=pl_from AND page_namespace=0 AND page_title IN ($qs) AND pl_namespace=120 AND pl_title IN ('".implode("','",array_keys($props))."') GROUP BY pl_title ORDER BY cnt DESC" ;
if(!$result = $dbwd->query($sql)) die('There was an error running the query [' . $dbwd->error . ']');
while($o = $result->fetch_object()) {
	$o->perc = $o->cnt * 100 / $ov->total_valid ;
	if ( $o->perc < $min_perc_props ) break ;
	$propcount[] = $o ;
}
$result->free();

// Get sitelinks
$sitelinks = array() ;
$sql = "SELECT ips_site_id,count(*) AS cnt FROM wb_items_per_site WHERE ips_item_id IN ($items) GROUP BY ips_site_id ORDER BY cnt DESC" ;
if(!$result = $dbwd->query($sql)) die('There was an error running the query [' . $dbwd->error . ']');
while($o = $result->fetch_object()) {
	$o->perc = $o->cnt * 100 / $ov->total_valid ;
	if ( $o->perc < $min_perc_sitelinks ) break ;
	$sitelinks[] = $o ;
}
$result->free();

$sitelink_counts = array() ;
$sitelink_counts[0] = $ov->total_valid ;
$sql = "SELECT ips_item_id,count(*) AS cnt FROM wb_items_per_site WHERE ips_item_id IN ($items) GROUP BY ips_item_id" ;
if(!$result = $dbwd->query($sql)) die('There was an error running the query [' . $dbwd->error . ']');
while($o = $result->fetch_object()) {
	$cnt = $o->cnt*1 ;
	if ( $cnt >= 100 ) $cnt = 100 ;
	else if ( $cnt < 100 and $cnt >= 50 ) $cnt = 50 ;
	$sitelink_counts[$cnt]++ ;
	$sitelink_counts[0]-- ;
}

print "<h2>{$cat->name} <small>{$cat->desc}</small></h2>" ;
print "<p>Numbers are based on Mix'n'match assignments of Wikidata items. Linked lists may differ slightly in numbers.</p>" ;

$pc = getCatPropClaim() ;

$stats1 = array (
	'Total' => $ov->total ,
	'Assigned' => $ov->manual ,
	'Not assigned' => $ov->noq+$ov->autoq ,
	'No Wikidata item' => $ov->nowd ,
	'Not applicable to Wikidata' => $ov->na ,
//	'WD: Is human' => countWDQ ( 'claim[31:5]' ) ,
	'WD: Has "instance of"' => array ( countProps ( 'P31' ) , autolist("$pc AND claim[31]") ) ,
	'WD: Has "gender"' => array ( countProps ( 'P21' ) , autolist("$pc AND claim[21]") )
) ;

print "<h3>Overview</h3>" ;
print "<table class='table table-striped'><tbody>" ;
foreach ( $stats1 AS $k => $v ) {
	$cnt = $v ;
	if ( is_array ( $v ) ) $cnt = $v[0] ;
	print "<tr><th>$k</th>" ;
	
	if ( is_array ( $v ) ) {
		print "<td style='text-align:right;font-family:Courier'>" ;
		print "<a target='_blank' href='" . $v[1] . "'>" ;
		print number_format($cnt,0) . "</a></td>" ;
	} else {
		print "<td style='text-align:right;font-family:Courier'>" . number_format($cnt,0) . "</td>" ;
	}
	
	if ( $k == 'Total' ) print "<td></td></tr>" ;
	else print "<td style='text-align:right;font-family:Courier'>" . number_format($cnt*100/$ov->total_valid,1) . " %</td></tr>" ;
}
print "</tbody></table>" ;

print "<h3>Media and Authority Control properties (>=$min_perc_props% of items)</h3>" ;
print "<table class='table table-striped'><tbody>" ;
foreach ( $propcount AS $o ) {
	print "<tr><th>" . $props[$o->pl_title] . "</td>" ;
	print "<td style='text-align:right;font-family:Courier'><a target='_blank' href='" . autolist("$pc AND claim[".preg_replace('/\D/','',$o->pl_title)."]") . "'>" . number_format($o->cnt,0) . "</a></td>" ;
	print "<td style='text-align:right;font-family:Courier'>" . number_format($o->perc,1) . " %</td></tr>" ;
}
print "</tbody></table>" ;

print "<h3>Sitelinks (>=$min_perc_sitelinks% of items)</h3>" ;
print "<table class='table table-striped'><tbody>" ;
foreach ( $sitelinks AS $o ) {
	$url = autolist ( "$pc AND link[" . $o->ips_site_id . "]" ) ;
	print "<tr><th>{$o->ips_site_id}</td>" ;
	print "<td style='text-align:right;font-family:Courier'><a target='_blank' href='$url'>" . number_format($o->cnt,0) . "</a></td>" ;
	print "<td style='text-align:right;font-family:Courier'>" . number_format($o->perc,1) . " %</td></tr>" ;
}
print "</tbody></table>" ;


print "<h3>Number of Sitelinks per item</h3>" ;
print "<table class='table table-striped'><thead><tr><th>Sitelinks</th><th>Items</th><th>%</th></tr></thead><tbody>" ;
for ( $cnt = 100 ; $cnt >= 0 ; $cnt-- ) {
	if ( !isset($sitelink_counts[$cnt]) ) continue ;
	$pre = '' ;
	if ( $cnt == 50 or $cnt == 100 ) $pre = '≥&nbsp;' ;
	print "<tr>" ;
	print "<td style='text-align:right;font-family:Courier'>$pre" . number_format($cnt) . "</td>" ;
	print "<td style='text-align:right;font-family:Courier'>" . number_format($sitelink_counts[$cnt],0) . "</td>" ;
	print "<td style='text-align:right;font-family:Courier'>" . number_format($sitelink_counts[$cnt]*100/$ov->total_valid,1) . " %</td>" ;
	print "</tr>" ;
}
print "</tbody></table>" ;

print get_common_footer() ;

?>