#!/usr/bin/php
<?PHP

require_once ( 'public_html/php/common.php' ) ;
error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);

$db = openToolDB ( 'mixnmatch_p' , 'wikidatawiki.labsdb' , 'p50380g50851' ) ;
$db->set_charset("utf8") ;

$parts = array() ;
$sql = "SELECT * FROM entry WHERE catalog=29 and q is null" ;
#$sql .= " LIMIT 1" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
while($o = $result->fetch_object()){
	$parts[$o->id] = $o->ext_url ;
}

foreach ( $parts AS $id => $url ) {
	$html = str_replace ( "\n" , ' ' , file_get_contents ( $url ) ) ;
	if ( !preg_match ( '/Amsterdam, Rijksmuseum, inv.\/cat.nr.\s*(.+?)\s*<\/div>/' , $html , $m ) ) continue ;
	$n = str_replace ( ' ' , '-' , $m[1] ) ;
	$n = strtoupper ( $n ) ;
	if ( preg_match ( '/^A-\d+$/' , $n ) ) $n = "SK-$n" ;
	$wdq = "http://wdq.wmflabs.org/api?q=" . urlencode('CLAIM[195:190804] AND STRING[217:"'.$n.'"]') ;
#	print "$wdq\n" ;
	$j = json_decode ( file_get_contents ( $wdq ) ) ;
	if ( count($j->items) != 1 ) {
		print count($j->items) . " WDQ items for $n\n" ;
		continue ;
	}
	$q = $j->items[0] ;
	$ts = date ( 'YmdHis' ) ;
	$sql = "UPDATE entry SET q=$q,user=0,timestamp='$ts' WHERE id=$id AND q is null" ;
	print "$sql\n" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
}

?>