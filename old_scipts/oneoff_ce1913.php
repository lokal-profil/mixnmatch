#!/usr/bin/php
<?PHP

require_once ( 'public_html/php/common.php' ) ;
error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);

$db = openToolDB ( 'mixnmatch_p' , 'wikidatawiki.labsdb' , 'p50380g50851' ) ;

$parts = array() ;
$sql = "SELECT * FROM entry WHERE catalog=3 AND ext_desc=''" ;
#$sql .= " and q=252282" ; # TESTING
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
while($o = $result->fetch_object()){
	$parts[] = $o ;
}

foreach ( $parts AS $o ) {
	$url = str_replace ( ' ' , '_' , $o->ext_url ) . "?action=render" ;
	$rows = file_get_contents ( $url ) ;
	$rows = preg_replace ( '/ b\. /' , ' born ' , $rows ) ;
	$rows = preg_replace ( '/ d\. /' , ' died ' , $rows ) ;
	$rows = explode ( "\n" , $rows ) ;
	print $o->ext_name . "\n" ;
#	print_r ( $rows ) ;
	foreach ( $rows AS $r ) {
		if ( !preg_match ( '/\bborn\b/' , $r ) ) continue ;
		$r = preg_replace ( '/<.+?>/' , '' , $r ) ;
		$r = explode ( '. ' , $r ) ;
		$r = trim ( $r[0] ) ;
		if ( $r == '' ) continue ;
		$sql = "UPDATE entry SET ext_desc='" . $db->real_escape_string($r) . "' WHERE ext_desc='' and id=" . $o->id ;
#		print "$sql\n" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
		break ;
	}
}

?>