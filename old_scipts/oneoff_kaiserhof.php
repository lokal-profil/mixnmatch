#!/usr/bin/php
<?PHP
require_once ( 'public_html/php/common.php' ) ;

$db = openToolDB ( 'mixnmatch_p' , 'wikidatawiki.labsdb' , 'p50380g50851' ) ;

$sql = "SELECT * FROM entry WHERE catalog=54 AND (q is null or q < 1 or user=0)" ;
#$sql .= " and ext_id='10004'" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
$candidates = array() ;
while($o = $result->fetch_object()){
	$candidates[] = $o ;
}


foreach ( $candidates AS $o ) {
	$t = file_get_contents ( $o->ext_url ) ;
	if ( !preg_match ( '/<a href="http:\/\/viaf.org\/viaf\/(\d+)\/"/' , $t , $d ) ) continue ;
	$viaf = $d[1] ;
	$sql = "INSERT IGNORE INTO auxiliary (entry_id,aux_p,aux_name) values (" . $o->id . ",214,'$viaf')" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
#	print "$sql\n" ;
}

?>