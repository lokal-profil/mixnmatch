#!/usr/bin/php
<?PHP

require_once ( 'public_html/php/common.php' ) ;
error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);

$db = openToolDB ( 'mixnmatch_p' , 'wikidatawiki.labsdb' , 'p50380g50851' ) ;
$db->set_charset("utf8") ;

$parts = array() ;
$sql = "SELECT * FROM entry WHERE catalog=19" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
while($o = $result->fetch_object()){
#	if ( $n == $o->ext_id ) continue ;
#	$parts[$o->id] = $n ;
#	print "$n\t" . $o->ext_id . "\n" ;
	if ( !preg_match ( '/\?title=(.+)$/' , $o->ext_url , $m ) ) continue ;
	$n = urldecode ( str_replace ( '_' , ' ' , $m[1] ) ) ;
#	$n = utf8_encode ( $n ) ;
	$parts[$o->id] = $n ;
}

foreach ( $parts AS $k => $v ) {
	$sql = "UPDATE entry SET ext_id='" . $db->real_escape_string($v) . "' WHERE id=$k" ;
#	print "$sql\n" ;
	if(!$result = $db->query($sql)) print('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
}

?>