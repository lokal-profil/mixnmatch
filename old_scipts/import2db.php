#!/usr/bin/php
<?PHP

// FORMAT : [ID TAB] title TAB desc [TAB url] [TAB Q]

include_once ( 'public_html/php/common.php' ) ;

$catalog = 34 ;
$id_first_column = true ;

function getNext ( &$parts ) {
	global $db ;
	return $db->real_escape_string ( html_entity_decode ( trim ( array_shift ( $parts ) ) ) ) ;
}

$db = openToolDB ( 'mixnmatch_p' , 'wikidatawiki.labsdb' , 'p50380g50851' ) ;
$db->autocommit ( false ) ;
//$db->begin_transaction() ;

$f = fopen( 'php://stdin', 'r' );

$q = '' ;
$ts = date ( 'YmdHis' ) ;
$id = 1 ;
while( $line = fgets( $f ) ) {
	if ( trim ( $line ) == '' ) continue ;
	$parts = explode ( "\t" , trim ( $line ) ) ;
	while ( count ( $parts ) < 5 ) $parts[] = '' ;
	if ( $id_first_column ) $id = getNext($parts) ; //$db->real_escape_string ( trim ( array_shift ( $parts ) ) ) ;
	else $id++ ;
	$title = getNext($parts) ; //$db->real_escape_string ( trim ( array_shift ( $parts ) ) ) ;
	$desc = getNext($parts) ; //$db->real_escape_string ( trim ( array_shift ( $parts ) ) ) ;
	$url = getNext($parts) ; //$db->real_escape_string ( trim ( array_shift ( $parts ) ) ) ;
//	$q = getNext($parts) ; //$db->real_escape_string ( trim ( array_shift ( $parts ) ) ) ;
	
	$sql = "INSERT IGNORE INTO entry (catalog,ext_id,ext_url,ext_name,ext_desc" ;
	if ( $q != '' ) $sql .= ",q,user,timestamp" ;
	$sql .= ") VALUES ($catalog," ;
	$sql .= '"' . $db->real_escape_string ( $id ) . '",' ;
	$sql .= '"' . $db->real_escape_string ( $url ) . '",' ;
	$sql .= '"' . $db->real_escape_string ( ($title) ) . '",' ;
	$sql .= '"' . $db->real_escape_string ( ($desc) ) . '"' ;
	if ( $q != '' ) $sql .= ",'" . $db->real_escape_string ( $q ) . "',3,'$ts'" ;
	$sql .= ')' ;
	
//	print "$sql\n" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
}

fclose( $f );

$sql = "update entry set random=rand() where random is null" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']: '.$sql."\n");

$db->commit() ;
$db->autocommit ( true ) ;
$db->close() ;


?>
