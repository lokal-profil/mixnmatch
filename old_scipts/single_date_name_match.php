#!/usr/bin/php
<?PHP

$catalog = 16 ; // Unset for "all"

require_once ( 'public_html/php/common.php' ) ;

$db = openToolDB ( 'mixnmatch_p' , 'wikidatawiki.labsdb' , 'p50380g50851' ) ;

$cnt = 0 ;
$sql = "select * from entry where (user=0 or user is null) and ext_desc LIKE '%(%)%'" ;
if ( isset ( $catalog ) ) $sql .= " AND catalog=$catalog" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	if ( !preg_match ( '/\b(\d{3,4})\b/' , $o->ext_desc , $m ) ) continue ;
	if ( $m[1]*1>2020 ) continue ; // Paranoia
	$y1 = $m[1] ;

//	print "--------------\n\n" ;	
//	print_r ( $o ) ;

	$sql = "SELECT DISTINCT terms.term_entity_id AS i FROM wikidatawiki_p.wb_terms AS terms WHERE " ;
	$sql .= " terms.term_type IN ('label','alias') and terms.term_text='" . $db->real_escape_string($o->ext_name) . "' and terms.term_entity_type='item'" ;
	$sql .= " and term_language='en' " ;
//	print "$sql\n" ;
	if(!$result2 = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	$items = array() ;
	while($o2 = $result2->fetch_object()){
		$items[$o2->i] = $o2->i ;
	}
//	print_r ( $items ) ;
	
	$query = "between[569,$y1-00-00,$y1-13-31] or between[570,$y1-00-00,$y1-13-31]" ;
//	print "$y1\t$y2\t$query\n" ;
	$url = "$wdq_internal_url?q=" . urlencode($query) ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
//	print_r ( $j ) ;
	
	$candidates = array() ;
	foreach ( $j->items AS $k => $v ) {
		if ( isset ( $items[$v] ) ) $candidates[] = $v ;
	}
	
	if ( count ( $candidates ) != 1 ) continue ;
	$q = array_pop ( $candidates ) ;
	
	$ts = date ( 'YmdHis' ) ;
	$sql = "UPDATE entry SET user=0,q=$q,timestamp='$ts' WHERE id=" . $o->id ;
	if(!$result2 = $db->query($sql)) die('There was an error running the query [' . $db->error . ']: '.$sql);
	
	print $o->id . " => Q$q\n" ;
	
//	print "$sql\n" ;

	$cnt++ ;
//	break ;
}

print "$cnt assigned\n" ;

// Cleanup
$sql = "update entry,wikidatawiki_p.wb_entity_per_page epp,wikidatawiki_p.pagelinks set timestamp=null,user=null,q=null where pl_from=epp_page_id and epp_entity_type='item' and pl_namespace=0 and pl_title='Q4167410' and epp_entity_id=q and user=0" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'.":$sql\n");


?>
