#!/usr/bin/perl

use strict ;
use warnings ;

open OUT , ">list2.tab" ;
open FILE , 'list.tab' ;
while ( <FILE> ) {
	chomp ;
	my ( $id , $url , $label ) = split "\t" , $_ ;
	next unless defined $label ;
	my $s = `curl -s -L $url | grep headword | grep span | head -1` ;
	chomp $s ;
	$s =~ s|<span.*?>||g ;
	$s =~ s|</span>||g ;
	print OUT "$id\t$url\t$label\t$s\n" ;
}
close FILE ;
close OUT ;
