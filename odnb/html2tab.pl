#!/usr/bin/perl

use strict ;
use warnings ;

while ( <> ) {
	next unless $_ =~ m|^<li><a href="/index/(\d+)/(.+?)">(.+?)</a></li>$| ;
	print "$1\thttp://www.oxforddnb.com/index/$1/$2\t$3\n" ;
}
